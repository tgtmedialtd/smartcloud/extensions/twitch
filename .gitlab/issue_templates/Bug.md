## Summary

<!-- Summarize the bug encountered concisely -->

<!-- If the issue is related to any other issue, please link it here -->
/relate #1

## Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

### Example Project

<!-- If possible, please create an example project here on GitLab.com that exhibits the problematic behaviour, and link to it here in the bug report

If you are using an older version of GitLab, this will also determine whether the bug has been fixed in a more recent version -->


## What is the current bug behaviour?

<!-- What actually happens -->


## What is the expected correct behaviour?

<!-- What you should see instead -->


## Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks ``` to format console output,
logs, and code as it's very hard to read otherwise. -->


## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

## Bug Checklist

* [ ] Have reproduced on my application version
* [ ] Have reproduced on clean installation
* [ ] Have reproduced on development build
* [ ] Have included logs or screenshots
* [ ] Have contacted support via smartcloud official website
* [ ] Have asked the community for guidance
* [ ] Have linked any related issues

/label ~bug::new ~platform::
/assign <!--@tgtmedialtd/smartcloud/frontend--> <!--@tgtmedialtd/smartcloud/central--> <!--@tgtmedialtd/smartcloud/application--> <!--@tgtmedialtd/smartcloud/security--> <!--@tgtmedialtd/smartcloud/extensions-->
